package autoload

/*
	You can just read the .env file on import just by doing

		import _ "gitlab.com/floodcode/godotenv/autoload"

	And bob's your mother's brother
*/

import "gitlab.com/floodcode/godotenv"

func init() {
	godotenv.Load()
}
